#!/usr/bin/env python3
import os
import rospy
import numpy as np
from my_package import Duckiebot
import torch
import torchvision
import cv2
import torch.nn.functional as F
import time

def preprocess(camera_value):
    global device, normalize
    x = camera_value
    x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
    x = x.transpose((2, 0, 1))
    x = torch.from_numpy(x).float()
    x = normalize(x)
    x = x.to(device)
    x = x[None, ...]
    return x


def process_image(img):
    dec_img = duckie.decode_image(img)
    x = dec_img
    x = preprocess(x)
    y = model(x)
    # we apply the `softmax` function to normalize the output vector so it sums to 1 (which makes it a probability distribution)
    y = F.softmax(y, dim=1)
    prob_blocked = float(y.flatten()[0])
    
    if prob_blocked < 0.5:
        duckie.publish_wheel_cmd(0.3, 0.3)
    else:
        duckie.publish_wheel_cmd(0.3, 0.2)

if __name__ == '__main__':

    global device, normalize


    model = torchvision.models.alexnet(pretrained=False)
    model.classifier[6] = torch.nn.Linear(model.classifier[6].in_features, 2)

    model.load_state_dict(torch.load('/data/best_model.pth'))

    device = torch.device('cuda')
    model = model.to(device)

    mean = 255.0 * np.array([0.485, 0.456, 0.406])
    stdev = 255.0 * np.array([0.229, 0.224, 0.225])

    normalize = torchvision.transforms.Normalize(mean, stdev)
     # create the node
    duckie = Duckiebot(node_name='my_node')
    # connect camera
    duckie.connect_camera(process_image)

    duckie.publish_wheel_cmd(0, 0)
    # keep spinning
    rospy.spin()